﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ExceptionHelpers.cs" company="Email Hippo Ltd">
//   © Email Hippo Ltd
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace EMH.Lib.Patterns.ExceptionProcessor.Interface.Helpers
{
    using System;
    using System.Runtime.ExceptionServices;

    /// <summary>
    /// The exception helpers.
    /// </summary>
    public static class ExceptionHelpers
    {
        /// <summary>
        /// The rethrow.
        /// </summary>
        /// <typeparam name="TException">The type of the exception.</typeparam>
        /// <param name="exception">The exception.</param>
        public static void Rethrow<TException>(this TException exception)
            where TException : Exception
        {
            ExceptionDispatchInfo.Capture(exception).Throw();
        }
    }
}