﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ExceptionProcessorPolicies.cs" company="Email Hippo Ltd">
//   © Email Hippo Ltd
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace EMH.Lib.Patterns.ExceptionProcessor.Interface
{
    /// <summary>
    /// The exception processor policies.
    /// </summary>
    public enum ExceptionProcessorPolicies
    {
        /// <summary>
        /// The none.
        /// </summary>
        None = 0,

        /// <summary>
        /// The log and resume policy.
        /// </summary>
        LogAndResumePolicy,

        /// <summary>
        /// The log and throw policy.
        /// </summary>
        LogAndThrowPolicy,

        /// <summary>
        /// The log throw and alert policy.
        /// </summary>
        LogThrowAndAlertPolicy,

        /// <summary>
        /// The input output tier policy.
        /// </summary>
        InputOutputTierPolicy,

        /// <summary>
        /// The business tier policy.
        /// </summary>
        BusinessTierPolicy,

        /// <summary>
        /// The middle ware service tier policy.
        /// </summary>
        MiddleWareServiceTierPolicy,

        /// <summary>
        /// The presentation tier policy.
        /// </summary>
        PresentationTierPolicy,

        /// <summary>
        /// The service host tier policy.
        /// </summary>
        ServiceHostTierPolicy,

        /// <summary>
        /// The WCF exception shielding policy.
        /// </summary>
        WcfExceptionShieldingPolicy
    }
}