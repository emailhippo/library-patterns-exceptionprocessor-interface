﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IExceptionProcessor.cs" company="Email Hippo Ltd">
//   © Email Hippo Ltd
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace EMH.Lib.Patterns.ExceptionProcessor.Interface
{
    using System;
    using System.Threading.Tasks;

    /// <summary>
    /// The ExceptionProcessor interface.
    /// </summary>
    public interface IExceptionProcessor
    {
        /// <summary>
        /// The handle exception.
        /// </summary>
        /// <typeparam name="TException">The type of the exception.</typeparam>
        /// <param name="exception">The exception.</param>
        /// <param name="policy">The policy.</param>
        /// <param name="className">Name of the class.</param>
        /// <param name="methodName">Name of the method.</param>
        /// <returns>
        /// The True if the exception should re-throw
        /// </returns>
        bool HandleException<TException>(
            TException exception,
            ExceptionProcessorPolicies policy = ExceptionProcessorPolicies.LogAndThrowPolicy,
            string className = "No Class Set",
            string methodName = "No Method Set")
            where TException : Exception;

        /// <summary>
        /// Logs the exception.
        /// </summary>
        /// <typeparam name="TException">The type of the exception.</typeparam>
        /// <param name="exception">The exception.</param>
        /// <param name="className">Name of the class.</param>
        /// <param name="methodName">Name of the method.</param>
        void LogException<TException>(
            TException exception,
            string className = "No Class Set",
            string methodName = "No Method Set")
            where TException : Exception;

        /// <summary>
        /// Processes the specified action.
        /// </summary>
        /// <typeparam name="TResponse">The type of the response.</typeparam>
        /// <param name="action">The action.</param>
        /// <param name="policy">The policy.</param>
        /// <param name="className">Name of the class.</param>
        /// <param name="methodName">Name of the method.</param>
        /// <returns>
        /// The TResponse
        /// </returns>
        TResponse Process<TResponse>(
            Func<TResponse> action,
            ExceptionProcessorPolicies policy = ExceptionProcessorPolicies.LogAndThrowPolicy,
            string className = "No Class Set",
            string methodName = "No Method Set");

        /// <summary>
        /// Processes the specified action.
        /// </summary>
        /// <typeparam name="TResponse">The type of the response.</typeparam>
        /// <param name="action">The action.</param>
        /// <param name="defaultResponse">The default response.</param>
        /// <param name="policy">The policy.</param>
        /// <param name="className">Name of the class.</param>
        /// <param name="methodName">Name of the method.</param>
        /// <returns>
        /// The TResponse
        /// </returns>
        TResponse Process<TResponse>(
            Func<TResponse> action,
            TResponse defaultResponse,
            ExceptionProcessorPolicies policy = ExceptionProcessorPolicies.LogAndThrowPolicy,
            string className = "No Class Set",
            string methodName = "No Method Set");

        /// <summary>
        /// Processes the specified action.
        /// </summary>
        /// <param name="action">The action.</param>
        /// <param name="policy">The policy.</param>
        /// <param name="className">Name of the class.</param>
        /// <param name="methodName">Name of the method.</param>
        void Process(
            Action action,
            ExceptionProcessorPolicies policy = ExceptionProcessorPolicies.LogAndThrowPolicy,
            string className = "No Class Set",
            string methodName = "No Method Set");

        /// <summary>
        /// Processes the asynchronous.
        /// </summary>
        /// <typeparam name="TResponse">The type of the response.</typeparam>
        /// <param name="action">The action.</param>
        /// <param name="policy">The policy.</param>
        /// <param name="className">Name of the class.</param>
        /// <param name="methodName">Name of the method.</param>
        /// <returns>A <see cref="Task" /> representing the asynchronous operation.</returns>
        Task<TResponse> ProcessAsync<TResponse>(
            Func<Task<TResponse>> action,
            ExceptionProcessorPolicies policy = ExceptionProcessorPolicies.LogAndThrowPolicy,
            string className = "No Class Set",
            string methodName = "No Method Set");

        /// <summary>
        /// Processes the asynchronous.
        /// </summary>
        /// <typeparam name="TResponse">The type of the response.</typeparam>
        /// <param name="action">The action.</param>
        /// <param name="defaultResponse">The default response.</param>
        /// <param name="policy">The policy.</param>
        /// <param name="className">Name of the class.</param>
        /// <param name="methodName">Name of the method.</param>
        /// <returns>
        /// The TResponse
        /// </returns>
        Task<TResponse> ProcessAsync<TResponse>(
            Func<Task<TResponse>> action,
            TResponse defaultResponse,
            ExceptionProcessorPolicies policy = ExceptionProcessorPolicies.LogAndThrowPolicy,
            string className = "No Class Set",
            string methodName = "No Method Set");

        /// <summary>
        /// Processes the asynchronous.
        /// </summary>
        /// <param name="action">The action.</param>
        /// <param name="policy">The policy.</param>
        /// <param name="className">Name of the class.</param>
        /// <param name="methodName">Name of the method.</param>
        /// <returns>A <see cref="Task" /> representing the asynchronous operation.</returns>
        Task ProcessAsync(
            Func<Task> action,
            ExceptionProcessorPolicies policy = ExceptionProcessorPolicies.LogAndThrowPolicy,
            string className = "No Class Set",
            string methodName = "No Method Set");
    }
}