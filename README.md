# EMH.Lib.Patterns.ExceptionProcessor.Interface

# Interface for Exception Processor

install using myget
```
install-package EMH.Lib.Patterns.ExceptionProcessor.Interface
```

## Exception Manager Implementation

Install using MyGet

```
Install-Package EMH.Lib.Patterns.ExceptionProcessor

```

## Console Implementation (low configuration, writes out to console. Intended for unit tests)

Install using MyGet

```
Install-Package EMH.Lib.Patterns.ExceptionProcessor.Console


# Rethrow

Preserves the line that an exception was thrown on so that logging in the same try / catch block will gives the correct line number.
(Due to a 'feature' in C# where it only logs the line number of the throw;)

```charp
try
{
	// Some code here
	if(failed)
	{
		throw new InvalidOperationException("That wasn't supposed to happen...");
	}
	// Some more code
}
catch (Exception e)
{
	e.Rethrow(); // The stack trace will mark the line the was thrown on.
}
```